#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/float.h"     // Required for using single-precision variables.
#include "pico/double.h"    // Required for using double-precision variables.
#include "pico/multicore.h" // Required for using multiple cores on the RP2040.

/**
 * @brief This function acts as the main entry-point for core #1.
 *        A function pointer is passed in via the FIFO with one
 *        incoming int32_t used as a parameter. The function will
 *        provide an int32_t return value by pushing it back on 
 *        the FIFO, which also indicates that the result is ready.
 */
void core1_entry() {
    while (1) {
        // 
        int32_t (*func)() = (int32_t(*)()) multicore_fifo_pop_blocking();
        int32_t p = multicore_fifo_pop_blocking();
        int32_t result = (*func)(p);
        multicore_fifo_push_blocking(result);
    }
}

float singleWallis(int iterations){
    float currentNumber=(2.0/1.0)*(2.0/3.0); //initialise the running number so that the loop simply needs to add onto it
    for(float run = 2.0 ;run<=iterations;run++){
        float multiplier = ((2.0*run) / ((2.0*run)-1.0));
        multiplier*=((2.0*run) / ((2.0*run)+1.0));

        currentNumber=currentNumber*multiplier;
    }
    float calculatedPI=currentNumber*2;
    return calculatedPI;
}

double doubleWallis(int iterations){
    double currentNumber=(2.0/1.0)*(2.0/3.0); //initialise the running number so that the loop simply needs to add onto it
    for(double run = 2.0 ;run<=iterations;run++){
        double multiplier = ((2.0*run) / ((2.0*run)-1.0));
        multiplier*=((2.0*run) / ((2.0*run)+1.0));

        currentNumber=currentNumber*multiplier;
    }
    double calculatedPI=currentNumber*2;
    return calculatedPI;
}

// Main code entry point for core0.
int main() {

    const int    ITER_MAX   = 100000;

    stdio_init_all();
    
    printf("Sequential single-point run");
    int sequential_total_start = time_us_64();
    int sequential_single_start = time_us_64();
    singleWallis(ITER_MAX);
    int sequential_single_end = time_us_64();
    int sequential_single_total_time = sequential_single_end - sequential_single_start;
    


    printf("Sequential double-point run");
    int sequential_double_start = time_us_64();
    doubleWallis(ITER_MAX);
    int sequential_double_end = time_us_64();
    int sequential_double_total_time = sequential_double_end - sequential_double_start;
    int sequential_total_end = time_us_64();
    int sequential_total_time = sequential_total_end - sequential_total_start;
    printf("Single point sequential took %f microseconds",sequential_single_total_time);
    printf("Double point sequential took %f microseconds",sequential_double_total_time);
    printf("Total sequential time %f microseconds",sequential_total_time);

    // Code for sequential run goes here…
    //    Take snapshot of timer and store
    //    Run the single-precision Wallis approximation
    //    Run the double-precision Wallis approximation
    //    Take snapshot of timer and store
    //    Display time taken for application to run in sequential mode
    multicore_launch_core1(core1_entry);

    printf("parallel single-point run");
    int parallel_single_start = time_us_64();
    int parallel_total_start = time_us_64();
    multicore_fifo_push_blocking((uintptr_t) &singleWallis);
    multicore_fifo_push_blocking(ITER_MAX);
    

    int parralel_double_start = time_us_64();
    doubleWallis(ITER_MAX);
    int parallel_double_end = time_us_64();
    
    int parallel_double_total_time = parallel_double_end - parralel_double_start;
    
    multicore_fifo_pop_blocking(); //wait for the second core to finish
    int parallel_single_end = time_us_64();
    int parallel_single_total_time = parallel_single_end - parallel_single_start;
    int parallel_total_end = time_us_64();
    int parallel_total_time = parallel_total_end - parallel_total_start;
    printf("Single point parallel took %f microseconds",parallel_single_total_time);
    printf("Double point parallel took %f microseconds",parallel_double_total_time);
    printf("Total parallel time %f microseconds",parallel_total_time);


    // Code for parallel run goes here…
    //    Take snapshot of timer and store
    //    Run the single-precision Wallis approximation on one core
    //    Run the double-precision Wallis approximation on the other core
    //    Take snapshot of timer and store
    //    Display time taken for application to run in parallel mode

    return 0;
}