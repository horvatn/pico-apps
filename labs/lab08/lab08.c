// Must declare the main assembly entry point before use.
void main_asm();
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include "pico/stdlib.h"
#include "hardware/gpio.h"
#include "hardware/pio.h"
#include "hardware/clocks.h"
#include "hardware/watchdog.h"
#include "ws2812.pio.h"
// For RGB LED:
#define IS_RGBW true        // Will use RGBW format
#define NUM_PIXELS 1        // There is 1 WS2812 device in the chain
#define WS2812_PIN 28       // The GPIO pin that the WS2812 connected to.

/**
 * @brief LAB #08 - TEMPLATE
 *        Main entry point for the code - calls the main assembly
 *        function where the body of the code is implemented.
 * 
 * @return int      Returns exit-status zero on completion.
 */

// Function from RGB LED example. Generates composite RGB value.
static inline uint32_t urgb_u32(uint8_t r, uint8_t g, uint8_t b) {
    return  ((uint32_t) (r) << 8)  |
            ((uint32_t) (g) << 16) |
            (uint32_t) (b);
}

// Function from RGB LED example. Sends 32 bit colour value to LED.
static inline void put_pixel(uint32_t pixel_grb) {
    pio_sm_put_blocking(pio0, 0, pixel_grb << 8u);
}

void setColour(float temperatureValue){
    uint8_t r = 0;
    uint8_t g = 0;
    uint8_t b = 0;
    if(temperatureValue < 20){
        r=0;
        g=0;
        b=85;
    }
    else if(temperatureValue >= 20 && temperatureValue < 25){
        r=85;
        g=55;
        b=0;
    }
    else if(temperatureValue >= 25){
        r=85;
        g=0;
        b=0;
    }
    //create a variable for red and blue, each to be passed into the put_pixel function
    


    put_pixel(urgb_u32(r, g, b));
}

float calcTemperatureCelcius(int rawTemperature) {
    float Celsius = 437-((rawTemperature*100.0)/215.0);
    printf("Temperature in Celsius: %f\n", Celsius);
    setColour(Celsius);
    return Celsius;
}







int main() {
    // Initialise PIO interface from RGB example code.
    PIO pio = pio0;
    uint offset = pio_add_program(pio, &ws2812_program);
    ws2812_program_init(pio, 0, offset, WS2812_PIN, 800000, IS_RGBW);

    // Jump into the main assembly code subroutine.
    main_asm();

    // Returning zero indicates everything went okay.
    return 0;
}
