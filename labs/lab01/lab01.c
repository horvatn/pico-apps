#include "pico/stdlib.h"
int pin25State;
/**
 * @brief EXAMPLE - BLINK_C moved over 
 *        Simple example to initialise the built-in LED on
 *        the Raspberry Pi Pico and then flash it forever. 
 * 
 * @return int  Application return code (zero for success).
 */
void toggle(uint PIN, uint DELAY){
  if (pin25State==0){
    // Toggle the LED on and then sleep for delay period
    gpio_put(PIN, 1);
    sleep_ms(DELAY);
    pin25State = 1;
  }

  else{
    // Toggle the LED off and then sleep for delay period
    gpio_put(PIN, 0);
    sleep_ms(DELAY);
    pin25State = 0;
  }

}

int main() {
    pin25State = 0;
    // Specify the PIN number and sleep delay
    const uint LED_PIN   =  25;
    const uint LED_DELAY = 500;
    const uint LED_LONG_DELAY = 2000;

    // Setup the LED pin as an output.
    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);

    // Do forever...
    while (true) {
      toggle( LED_PIN,  LED_DELAY); //toggles quickly twice
      toggle( LED_PIN,  LED_DELAY);
      toggle( LED_PIN,  LED_LONG_DELAY); //toggles slowly once
    }

    // Should never get here due to infinite while-loop.
    return 0;

}
