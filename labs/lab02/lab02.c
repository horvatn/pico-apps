#define WOKWI             // Uncomment if running on Wokwi RP2040 emulator.

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//#include "pico/stdlib.h"
#include "pico/float.h"     // Required for using single-precision variables.
#include "pico/double.h"    // Required for using double-precision variables.


/**
 * @brief EXAMPLE - HELLO_C
 *        Simple example to initialise the IOs and then 
 *        print a "Hello World!" message to the console.
 * 
 * @return int  Application return code (zero for success).
 */
int main() {
  
  double iterations = 100000.0;
  double basis = 3.14159265359;
  double currentNumber=(2.0/1.0)*(2.0/3.0); //initialise the running number so that the loop simply needs to add onto it
  for(double run = 2.0 ;run<=iterations;run++){
    double multiplier = ((2.0*run) / ((2.0*run)-1.0));
    multiplier*=((2.0*run) / ((2.0*run)+1.0));

    currentNumber=currentNumber*multiplier;
  }
  double calculatedPI=currentNumber*2;


#ifndef WOKWI
    // Initialise the IO as we will be using the UART
    // Only required for hardware and not needed for Wokwi
    stdio_init_all();
#endif

    // Print a console message to inform user what's going on.
    printf("Pi is calculated to:%.11f\n",calculatedPI);
    printf("This has an approximation error of: %.11f for the double-precision representation\n",abs(calculatedPI-basis));
    // Returning zero indicates everything went okay.
    return 0;
}
