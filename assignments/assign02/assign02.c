// Assignment 2 - Group 10
// Gavin Milner
// Timofejs Cvetkovs
// Darryl Boggins
// Nicolas Quintana
// Nicholas Horvat

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include "pico/stdlib.h"
#include "hardware/gpio.h"
#include "hardware/pio.h"
#include "hardware/clocks.h"
#include "hardware/watchdog.h"
#include "ws2812.pio.h"
// #include "display/display_l2.h"

// ANSI escape codes
#define CLEAR_SCREEN "\x1b[2J\x1b[H"

// Define read_morse prototype
void read_morse(char *morse_string);

// Defining array size macros.
# define CHAR_ARRAY_SIZE 36

// Declare list of Letters in an array.
const char char_list[CHAR_ARRAY_SIZE] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 
'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

// Declare list of corresponding morse code at same index location.
const char *morselist[CHAR_ARRAY_SIZE] = {".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", 
"--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--..", "-----", ".----", "..---",
"...--", "....-", ".....", "-....", "--...", "---..", "----."};

const char* letter_patterns[] = {
    "                    # #   \n                   #   #  \n                  #     # \n                  ####### \n                  #     # \n                  #     # \n                 ", // A
    "                  ######   \n                  #     #  \n                  #     #  \n                  ######   \n                  #     #  \n                  #     #  \n                  ######   \n                 ", // B
    "                    #####  \n                   #       \n                  #        \n                  #        \n                  #        \n                   #       \n                    #####  \n                 ", // C
    "                  #####    \n                  #    #   \n                  #     #  \n                  #     #  \n                  #    #   \n                  #####    \n                 ", // D
    "                  #######  \n                  #        \n                  #        \n                  ######   \n                  #        \n                  #        \n                  #######  \n                 ", // E
    "                  #######  \n                  #        \n                  #        \n                  ######   \n                  #        \n                  #        \n                  #        \n                 ", // F
    "                    ###### \n                   #       \n                  #   #### \n                  #       #\n                  #       #\n                   #      #\n                    ##### #\n                 ", // G
    "                  #     #  \n                  #     #  \n                  #     #  \n                  #######  \n                  #     #  \n                  #     #  \n                  #     #  \n                 ", // H
    "                   #####   \n                     #     \n                     #     \n                     #     \n                     #     \n                     #     \n                   #####   \n                 ", // I
    "                        #  \n                        #  \n                        #  \n                        #  \n                  #     #  \n                   #####   \n                 ", // J
    "                  #    #   \n                  #   #    \n                  #  #     \n                  ##       \n                  #  #     \n                  #   #    \n                  #    #   \n                 ", // K
    "                  #        \n                  #       \n                  #       \n                  #       \n                  #       \n                  #       \n                  #######  \n                 ", // L
    "                  #     #  \n                  ##   ##  \n                  # # # #  \n                  #  #  #  \n                  #     #  \n                  #     #  \n                  #     #  \n                 ", // M
    "                  #     #  \n                  ##    #  \n                  # #   #  \n                  #  #  #  \n                  #   # #  \n                  #    ##  \n                  #     #  \n                 ", // N
    "                    ###    \n                   #   #   \n                  #     #  \n                  #     #  \n                   #   #   \n                    ###    \n                 ", // O
    "                  ######   \n                  #     #  \n                  #     #  \n                  ######   \n                  #        \n                  #        \n                  #        \n                 ", // P
    "                    ###    \n                   #   #   \n                  #     #  \n                  #  #  #  \n                   # # #   \n                    ###  # \n                        #  \n                 ", // Q
    "                  ######   \n                  #     #  \n                  #     #  \n                  ######   \n                  #   #    \n                  #    #   \n                  #     #  \n                 ", // R
    "                   #####   \n                  #       \n                   #####   \n                       #   \n                  #    #   \n                   ####    \n                 ", // S
    "                  #######  \n                     #     \n                     #     \n                     #     \n                     #     \n                     #     \n                     #     \n                 ", // T
    "                  #     #  \n                  #     #  \n                  #     #  \n                  #     #  \n                  #     #  \n                   #   #   \n                    ###    \n                 ", // U
    "                  #     #  \n                  #     #  \n                  #     #  \n                   #   #   \n                    # #    \n                     #     \n                     #     \n                 ", // V
    "                  #     #  \n                  #     #  \n                  #     #  \n                  #  #  #  \n                  # # # #  \n                  ##   ##  \n                  #     #  \n                 ", // W
    "                  #     #  \n                   #   #   \n                    # #    \n                     #     \n                    # #    \n                   #   #   \n                  #     #  \n                 ", // X
    "                  #     #  \n                   #   #   \n                    # #    \n                     #     \n                     #     \n                     #     \n                     #     \n                 ", // Y
    "                  #######  \n                       #   \n                      #    \n                     #     \n                    #      \n                   #       \n                  #######  \n                 ", // Z
    "                   ###  \n                  #   # \n                  #  ## \n                  # # # \n                  ##  # \n                  #   # \n                   ###  \n                 ", // 0
    "                     #   \n                    ##   \n                   # #   \n                     #   \n                     #   \n                     #   \n                 ####### \n                 ", // 1
    "                   ####  \n                  #    # \n                       # \n                    ###  \n                   #     \n                  #      \n                 ####### \n                 ", // 2
    "                  ####\n                      #\n                  ####\n                      #\n                      #\n                  ####", // 3      
    "                    ##   \n                   # #   \n                  #  #   \n                 ####### \n                     #   \n                     #   \n                     #   \n                 ", // 4
    "                  ###### \n                  #      \n                  #      \n                  ###### \n                       # \n                  #    # \n                   ####  \n                 ", // 5
    "                    ###  \n                   #     \n                  #      \n                  ###### \n                  #    # \n                  #    # \n                   ####  \n                 ", // 6
    "                 ####### \n                      #  \n                     #   \n                    #    \n                   #     \n                  #      \n                  #      \n                 ", // 7
    "                   ####  \n                  #    # \n                  #    # \n                   ####  \n                  #    # \n                  #    # \n                   ####  \n                 ", // 8
    "                   #####  \n                  #     # \n                  #     # \n                   #####  \n                       #  \n                  #    #  \n                   ####   \n                 ", // 9
};

// Declare the corresponding Morse Code for each letter 

// For RGB LED:
#define IS_RGBW true        // Will use RGBW format
#define NUM_PIXELS 1        // There is 1 WS2812 device in the chain
#define WS2812_PIN 28       // The GPIO pin that the WS2812 connected to.

// Must declare the main assembly entry point before use.
void main_asm();

void set_watchdog_timer(){
    // Set the watchdog timer to the maximum time of 9 seconds.
    // This is 8.3 x 1 x 10^6, the clock speed of the rp2040 processor.
    // 0x7fffff is the max delay time in hex, = 8388607.
    // Setting the second parameter to 1 will pause the watchdog when in debug mode.
    watchdog_enable(8388607, 1);
}

void asm_gpio_init(uint pin) {
    gpio_init(pin);
}

// Set direction of a GPIO pin see SDK for detail on gpio_set_dir()
void asm_gpio_set_dir(uint pin, bool out) {
    gpio_set_dir(pin, out);
}

// Get the value of a GPIO pin see SDK for detail on gpio_get()
bool asm_gpio_get(uint pin) {
    return gpio_get(pin);
}

// Set the value of a GPIO pin see SDK for detail on gpio_put()
void asm_gpio_put(uint pin, bool value) {
    gpio_put(pin, value);
}

// Enable falling-edge interrupt see SDK for detail on gpio_set_irq_enabled()
void asm_gpio_enable_irq_fall(uint pin) {
    gpio_set_irq_enabled(pin, GPIO_IRQ_EDGE_FALL, true);
}

// Disable falling-edge interrupt see SDK for detail on gpio_set_irq_enabled()
void asm_gpio_disable_irq_fall(uint pin) {
    gpio_set_irq_enabled(pin, GPIO_IRQ_EDGE_FALL, false);
}

// Enable rising-edge interrupt see SDK for detail on gpio_set_irq_enabled()
void asm_gpio_enable_irq_rise(uint pin) {
    gpio_set_irq_enabled(pin, GPIO_IRQ_EDGE_RISE, true);
}

// Disable rising-edge interrupt see SDK for detail on gpio_set_irq_enabled()
void asm_gpio_disable_irq_rise(uint pin) {
    gpio_set_irq_enabled(pin, GPIO_IRQ_EDGE_RISE, false);
}

void print_welcome(){

    printf("+-------------------------------------------+\n");
    printf("|                                           |\n");
    printf("|  #       #######    #    ######  #     #  |\n");
    printf("|  #       #         # #   #     # ##    #  |\n");
    printf("|  #       #        #   #  #     # # #   #  |\n");
    printf("|  #       #####   #     # ######  #  #  #  |\n");
    printf("|  #       #       ####### #   #   #   # #  |\n");
    printf("|  #       #       #     # #    #  #    ##  |\n");
    printf("|  ####### ####### #     # #     # #     #  |\n");
    printf("|                                           |\n");
    printf("|  #     # ####### ######   #####  #######  |\n");
    printf("|  ##   ## #     # #     # #     # #        |\n");
    printf("|  # # # # #     # #     # #       #        |\n");
    printf("|  #  #  # #     # ######   #####  #####    |\n");
    printf("|  #     # #     # #   #         # #        |\n");
    printf("|  #     # #     # #    #  #     # #        |\n");
    printf("|  #     # ####### #     #  #####  #######  |\n");
    printf("|                                           |\n");
    printf("+-------------------------------------------+\n");
    printf("|  USE GP21 TO ENTER A SEQUENCE TO BEGIN    |\n");
    printf("|     \"-----\" - LEVEL 01 - CHARS (EASY)     |\n");
    printf("|     \".----\" - LEVEL 02 - CHARS (HARD)     |\n");
    printf("|     \"..---\" - LEVEL 03 - WORDS (EASY)     |\n");
    printf("|     \"...--\" - LEVEL 04 - WORDS (HARD)     |\n");
    printf("+-------------------------------------------+\n");


}

// Function from RGB LED example. Sends 32 bit colour value to LED.
static inline void put_pixel(uint32_t pixel_grb) {
    pio_sm_put_blocking(pio0, 0, pixel_grb << 8u);
}

// Function from RGB LED example. Generates composite RGB value.
static inline uint32_t urgb_u32(uint8_t r, uint8_t g, uint8_t b) {
    return  ((uint32_t) (r) << 8)  |
            ((uint32_t) (g) << 16) |
            (uint32_t) (b);
}


// Using the set_pixel function and providing the relevant RGB 
// hex values to set each of the different colours required.
void turn_LED_Red(){
    // array of hex numbers to set intensity of each component.
    // Based on RGB colour table.
    // Colours set to half of the intensity.
    put_pixel(urgb_u32(0x7F, 0x00, 0x00));
}

void turn_LED_Orange(){
    put_pixel(urgb_u32(0x7F, 0x82, 0x00));
}

void turn_LED_Yellow(){
    put_pixel(urgb_u32(0x7F, 0x7F, 0x00));
}

void turn_LED_Blue(){
    put_pixel(urgb_u32(0x00, 0x00, 0x7F));
}

void turn_LED_Green(){
    put_pixel(urgb_u32(0x00, 0x7F, 0x00));
}

void turn_LED_Purple(){
    put_pixel(urgb_u32(0x35, 0x2D, 0x66));
}

void turn_LED_Turquoise(){
    put_pixel(urgb_u32(0x20, 0x70, 0x68));
}

void turn_LED_Black(){
    put_pixel(urgb_u32(0x0, 0x0, 0x0));
}
/*
 * Function to decode a string of morse characters into a string of letters and numbers.
 */
void decode_morse(char *input, char *output) {
    int i = 0;                         // Index of string 'input'.
    char currChar = input[i];          // Character corresponding to index 'i' of 'input'.
    int j = 0;                         // Index of current morse letter.
    char *currMorseLetter = malloc(6); // Current morse letter; 5 chars/bytes max morse size + null terminator.

    int finished = 0;
    while ( finished != 1 )
    {
        if ( currChar == '.' || currChar == '-' )
        {
            currMorseLetter[j] = currChar;
        }
        else if (currChar == ' ' || currChar == '\0') {     // Space/Null means we've reached the end of a morse letter,
            currMorseLetter[j] = '\0';                      // so we append the null terminator and process it.
            char append;        // Translated char (from morse) we will append to output.
            int k = 0;               
            while ( strcmp(currMorseLetter, morselist[k]) != 0 && k < 37)
            {
                k++;
            } // 0-25, a-x, 26-36, 0-9.
            if ( k < 37 ) {
                append = char_list[k];
            } else {
                append = '?';   // invalid character
            }
            strncat(output, &append, 1);
            j = -1; // This is set to -1 so it will become 0 on the next iteration,
        }           // allowing us to overwrite currMorseLetter.
        if ( currChar == '\0' ) {
            finished = 1; // no more characters after null, end the loop here
        } else {
            i++; // Increment and loop again.
            j++;
            currChar = input[i];
        }
    }
}

int level;
int level_counter;

int level1(int lives){
    int seed = time_us_32();

    for(int i = 0; i < 20; i++){
        srand(seed);
        seed += 1;

        int random_index = rand() % CHAR_ARRAY_SIZE;

        // printf("Random Character: %c\n", char_list[random_index]);
        printf("+-------------------------------------------+\n");
        printf("      What letter is this is morse code?     \n\n");
        printf("%s\n", letter_patterns[random_index]);
        printf("                 Hint: %s                    \n", morselist[random_index]);
        printf("+-------------------------------------------+\n");
        char morse_string[256];
        char decoded_string[256];
        read_morse(morse_string);
        decoded_string[0] = '\0';
        decode_morse(morse_string, decoded_string);

        if(decoded_string[0] == char_list[random_index]){
            printf("                   Correct!\n\n");
            if (level_counter < 5){
                level_counter += 1;
            }else{
                level_counter = 0; 
                level += 1;
            }

            if(lives < 3){
                lives =+ 1;
            }
            return lives;
        } else{
            printf("                  Incorrect!\n\n");
            lives -= 1;
            return lives;
        }
    }
}

int level2(int lives){
    int seed = time_us_32();

    for(int i = 0; i < 20; i++){
        srand(seed);
        seed += 1;

        int random_index = rand() % CHAR_ARRAY_SIZE;

        // printf("Random Character: %c\n", char_list[random_index]);
        printf("+-------------------------------------------+\n");
        printf("      What letter is this is morse code?     \n\n");
        printf("%s\n", letter_patterns[random_index]);
        printf("+-------------------------------------------+\n");
        char morse_string[256];
        char decoded_string[256];
        read_morse(morse_string);
        decoded_string[0] = '\0';
        decode_morse(morse_string, decoded_string);

        if(decoded_string[0] == char_list[random_index]){
            printf("                   Correct!\n\n");
            if (level_counter < 5){
                level_counter += 1;
            }else{
                level_counter = 0; 
                level += 1;
            }

            if(lives < 3){
                lives =+ 1;
            }
            return lives;
        } else{
            printf("                  Incorrect!\n\n");
            printf("                Answer: %s\n", morselist[random_index]);
            lives -= 1;
            return lives;
        }

    }
}

/*
 * Main entry point for the code - simply calls the main assembly function.
 */
int main() {

    stdio_init_all();              // Initialise all basic IO

    set_watchdog_timer();

    // If no activity is detected for the watchdog timer, the game will restart.
    if (watchdog_caused_reboot()){
        printf("Game Restarted due to Inactivity.\n");
    }

    // Initialise PIO interface from RGB example code.
    PIO pio = pio0;
    uint offset = pio_add_program(pio, &ws2812_program);
    ws2812_program_init(pio, 0, offset, WS2812_PIN, 800000, IS_RGBW);
    
    turn_LED_Green();
    level = 0;
    int lives = 3;
    char morse_string[256];
    char decoded_string[256];
    while(level != -1){
        switch (level) {
            case 0: {
                lives = 3;
                turn_LED_Green();
                print_welcome(); 
                read_morse(morse_string);
                decoded_string[0] = '\0';
                decode_morse(morse_string, decoded_string);
                if (strcmp(decoded_string, "0") == 0)
                    level = 1;
                else if (strcmp(decoded_string, "1") == 0)
                    level = 2;
                else if (strcmp(decoded_string, "9") == 0)
                    level = -1;
                else 
                    level = 0;
                break;
            }
            case 1:
                lives = level1(lives);
                break; 
            case 2:
                lives = level2(lives);
                break; 
        }
        // Print sucess message
        if( level == 3){
            printf("            You passed every level!\n\n");
            level = 0;
        }
        if(lives == 0){
            printf("               NO MORE LIVES!\n\n");
            turn_LED_Red();
            level = 0;
        }else if( lives == 1){
            turn_LED_Orange();
        }else if(lives == 2){
            turn_LED_Yellow();
        }else if( lives == 3){
            turn_LED_Green();
        }
    }
    return(0);
}
